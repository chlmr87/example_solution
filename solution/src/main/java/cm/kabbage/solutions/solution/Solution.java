package cm.kabbage.solutions.solution;

//you can also use imports, for example:
//import java.util.*;
//you can write to stdout for debugging purposes, e.g.
//System.out.println("this is a debug message");

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
	public int solution(String parameters) {
		String namePattern = "^[a-zA-Z]*$";
		Pattern pattern = Pattern.compile(namePattern);
		Matcher matcher;

		// gets rid of case checking
		parameters = parameters.toLowerCase();
		List<String> params = Arrays.asList(parameters.replaceFirst("--", "").split("--"));
		if(params.isEmpty() || null == params) {
			return -1;
		}
		
		int valid = 0;
		

		for (String item : params) {
			item = item.trim();
			

			if (item.startsWith("count")) {
				// assuming string is always formatted like this "count 44"
				int value = 0;

				try {
					value = Integer.valueOf(item.split("\\s")[1]);
				} catch (NumberFormatException nfe) {
					return -1;
				}
				if (value >= 10 && value <= 100) {
					continue;
				} else {
					return -1;
				}
			}

			if (item.startsWith("name")) {
				// assuming string is always formatted like count e.g. name
				// HelloWorld
				// also assumes what comes after name has no spaces
				String check = item.split("\\s")[1];
				matcher = pattern.matcher(check);
				if (matcher.matches()) {
					continue;
				} else {
					return -1;
				}
			}

			// if help is anywhere in the string and all the output is valid
			// will return 1 once complete.
			if (item.startsWith("help")) {
				valid = 1;
				continue;
			}

			// returns -1 if neither condition is met
			return -1;
		}
		return valid;
	}

}
