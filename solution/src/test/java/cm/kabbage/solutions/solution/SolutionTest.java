package cm.kabbage.solutions.solution;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SolutionTest {
	Solution solution = new Solution();
	
	@Test
	public void testValidCount() {
		assertEquals(0, solution.solution("--count 10"));
	}
	
	@Test
	public void testInvalidCount() {
		assertEquals(-1, solution.solution("--count 09"));
	}
	
	@Test
	public void testInvalidCountAlphaChars() {
		assertEquals(-1, solution.solution("--count XX"));
	}
	
	@Test
	public void testInvalidDoubleCount() {
		assertEquals(-1, solution.solution("--count 10.2"));
	}
	
	@Test
	public void testNameParameter() {
		assertEquals(0, solution.solution("--name HelloWorld"));
	}
	
	@Test
	public void testInvalidNameParameter() {
		assertEquals(-1, solution.solution("--name 99"));
	}
	
	@Test
	public void testValidNameAndCountParameters() {
		assertEquals(0, solution.solution("--count 10       --name yoyo"));
	}
	
	@Test
	public void testMultipleValidNameAndCountParameters() {
		assertEquals(0, solution.solution("--count 10   --count 100  --name MyNameIsChase  --name yoyo"));
	}
	
	@Test
	public void testMultipleInvalidNameAndCountParameters() {
		assertEquals(-1, solution.solution("--count 10   --count 101  --name MyNameIsChase  --name yoyo"));
	}
	
	@Test
	public void testHelpParameter() {
		assertEquals(1, solution.solution("--help"));
	}
	
	@Test
	public void testAllValidParameters() {
		assertEquals(1, solution.solution("--count 10 --name YoGabbaGabba      --help"));
	}
	
	@Test
	public void testEmptyParameters() {
		assertEquals(-1, solution.solution(""));
	}
	
	@Test
	public void testNoParameters() {
		assertEquals(-1, solution.solution("------------------"));
	}
	
	@Test
	public void testInvalidParameters() {
		assertEquals(-1, solution.solution("--help --pleh"));
	}
}
